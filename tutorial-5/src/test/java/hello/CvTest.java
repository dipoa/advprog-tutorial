package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CvTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void curriculumVitaeWithoutUser() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

    @Test
    public void curriculumVitaeWithUser() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "Dipo"))
                .andExpect(content().string(containsString("Dipo, I hope you interested to hire me")));
    }
}
