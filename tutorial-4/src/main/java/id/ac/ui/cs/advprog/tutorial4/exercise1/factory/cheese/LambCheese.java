package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class LambCheese implements Cheese {

    public String toString() {
        return "Garut Lamb Cheese";
    }
}
