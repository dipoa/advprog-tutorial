package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class CiliwungClams implements Clams {

    public String toString() {
        return "Not-So-Fresh Clams from Ciliwung River";
    }
}
