package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class BelibisSauce implements Sauce {
    public String toString() {
        return "Hot Belibis Sauce";
    }
}
