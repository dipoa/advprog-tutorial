package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        Food food = null;
        food = BreadProducer.THICK_BUN.createBreadToBeFilled();
        food = FillingDecorator.BEEF_MEAT.addFillingToBread(food);
        food = FillingDecorator.CHEESE.addFillingToBread(food);
        food = FillingDecorator.CHILI_SAUCE.addFillingToBread(food);
        food = FillingDecorator.CUCUMBER.addFillingToBread(food);
        food = FillingDecorator.LETTUCE.addFillingToBread(food);
        food = FillingDecorator.TOMATO.addFillingToBread(food);
        food = FillingDecorator.BEEF_MEAT.addFillingToBread(food);
        food = FillingDecorator.CHEESE.addFillingToBread(food);
        food = FillingDecorator.CHILI_SAUCE.addFillingToBread(food);
        food = BreadProducer.THIN_BUN.createBreadToBeFilled();

        System.out.println(food.getDescription());
        System.out.println("TOTAL COST: " + food.cost());
    }
}
