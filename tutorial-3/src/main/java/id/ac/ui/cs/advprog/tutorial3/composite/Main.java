package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Sio Mantul", 300000000));
        company.addEmployee(new Cto("Pak Tio", 2000000000));
        company.addEmployee(new BackendProgrammer("Bill Gates", 400000));
        company.addEmployee(new FrontendProgrammer("Steve Jobs", 300000));
        company.addEmployee(new NetworkExpert("Bima Tri", 300000));
        company.addEmployee(new UiUxDesigner("Fujiko F Fujio", 200000));
        company.addEmployee(new SecurityExpert("Dwayne Johnson", 400000));

        System.out.println("LIST OF EMPLOYEES:");
        for (Employees employees : company.getAllEmployees()) {
            System.out.println(employees.getName() + " [" + employees.getRole() + "]");
        }
        System.out.println(String.format("TOTAL SALARIES: %f", company.getNetSalaries()));
    }
}
