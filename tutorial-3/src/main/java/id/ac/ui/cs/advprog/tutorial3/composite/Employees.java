package id.ac.ui.cs.advprog.tutorial3.composite;

public abstract class Employees {
    protected String name = "Unidentified Name";
    protected double salary;
    protected String role;
    protected double salaryLowerBound;

    public Employees(String name, double salary, String role, double salaryLowerBound) {
        this.name = name;
        this.salary = salary;
        this.role = role;
        this.salaryLowerBound = salaryLowerBound;
        if (!this.isGoodSalary()) {
            throw new IllegalArgumentException(
                "Salary for " + role + " must not lower than " + salaryLowerBound
            );
        }
    }

    public String getName() {
        return this.name;
    }

    public abstract double getSalary();

    public String getRole() {
        return this.role;
    }

    public boolean isGoodSalary() {
        return this.salary >= this.salaryLowerBound;
    }

}
