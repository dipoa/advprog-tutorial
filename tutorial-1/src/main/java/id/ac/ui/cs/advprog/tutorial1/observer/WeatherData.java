package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        // TODO Complete me!
        return this.temperature;
    }

    public void setTemperature(float temperature) {
        // TODO Complete me!
        this.setMeasurements(temperature, this.humidity, this.pressure);
    }

    public float getHumidity() {
        // TODO Complete me!
        return this.humidity;
    }

    public void setHumidity(float humidity) {
        // TODO Complete me!
        this.setMeasurements(this.temperature, humidity, this.pressure);
    }

    public float getPressure() {
        // TODO Complete me!
        return this.pressure;
    }

    public void setPressure(float pressure) {
        // TODO Complete me!
        this.setMeasurements(this.temperature, this.humidity, pressure);
    }
}
